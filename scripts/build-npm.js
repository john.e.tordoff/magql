// ex. scripts/build-npm.ts
import { build, emptyDir } from "https://deno.land/x/dnt/mod.ts";
await emptyDir("./dist-npm");
await build({
    entryPoints: ["./lib/index.ts"],
    outDir: "./dist-npm",
    shims: {
        // see JS docs for overview and more options
        deno: true,
    },
    package: {
        // package.json properties
        name: "@magql/magql",
        version: Deno.args[0],
        description: "Your package.",
        license: "MIT",
        repository: {
            type: "git",
            url: "git+https://github.com/username/repo.git",
        },
        bugs: {
            url: "https://github.com/username/repo/issues",
        },
    },
});
// post build steps
Deno.copyFileSync("LICENSE", "npm/LICENSE");
Deno.copyFileSync("README.md", "npm/README.md");
