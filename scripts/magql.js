import path from "path";
import { execute } from "graphql";
import { gql } from "graphql-tag";
import { summonSchema } from "../lib/schema.ts";


import yargs from "yargs/yargs";
import { hideBin } from "yargs/helpers";


const { types: typesModulePath, query } = await yargs(hideBin(process.argv)).options({
    types: { type: "string", demandOption: true },
    query: { type: "string", demandOption: true }
}).argv;

const resolvedTypesModulePath = path.resolve("./", typesModulePath);
const { types } = await import(resolvedTypesModulePath);
const schema = summonSchema(types);

console.log(JSON.stringify(await execute({
    schema,
    document: gql(query)
}), null, 4));

