var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for(var i = decorators.length - 1; i >= 0; i--)if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { execute } from "graphql";
import { gql } from "graphql-tag";
import { query, field } from "../dist-esm/decorators.js";
import { summonSchema } from "../dist-esm/schema.js";
import { MaGQLType } from "../dist-esm/types/base.js";
let TestType = class TestType extends MaGQLType {
    static getOne() {
        return new TestType();
    }
    hello() {
        return "world";
    }
};
__decorate([
    query({
        operationName: "getOne",
        returns: {}
    })
], TestType, "getOne", null);
__decorate([
    field(String)
], TestType.prototype, "hello", null);
const schema = summonSchema([
    TestType
]);
const actual = await execute({
    schema,
    document: gql`
        query {
           getOne {
               hello
           }
        }
    `
});
const expected = {
    data: {
        getOne: {
            hello: "world"
        }
    }
};
console.log(actual, expected);


//# sourceMappingURL=demo-esm.es6.js.map