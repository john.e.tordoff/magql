use deno_bindgen::deno_bindgen;

#[deno_bindgen]
pub struct Input {
    a: i32,
    b: i32
}

#[deno_bindgen]
pub fn add(input: Input) -> i32 {
    input.a + input.b
}

#[deno_bindgen]
pub fn hello() -> String {
    "hello world".to_string()
}

//#[deno_bindgen]
//pub fn map_right<T, F>(map_fn: F, arr: T) -> T
//    where
//        T: IntoIterator,
//        T::Item: Copy,
//        F: Fn(T::Item) -> T::Item
//{
//    arr.into_iter().map(map_fn).collect()
//}
