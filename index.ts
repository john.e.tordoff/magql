import { GraphQLString, execute } from "graphql";
import { gql } from "graphql-tag";
import { query, field, returns } from "./lib/decorators.ts";
import { summonSchema } from "./lib/schema.ts";
import {
    MaGQLType,
    AnnotatedDescriptor
} from "./lib/types/base.ts";


import yargs from "yargs/yargs";
import { hideBin } from "yargs/helpers";


const { types, query } = await yargs(hideBin(process.argv)).options({
    types: { type: "string", demandOption: true },
    query: { type: "string", demandOption: true }
}).argv;

console.log({ types, query })


const schema = summonSchema([TestType]);

const actual = await execute({
    schema,
    document: gql`
        query {
           getOne {
               hello
           }
        }
    `
});

const expected = {
    data: {
        getOne: {
            hello: "world"
        }
    }
};

console.log(actual, expected);
