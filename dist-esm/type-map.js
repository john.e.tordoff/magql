import { GraphQLID, GraphQLBoolean, GraphQLFloat, GraphQLInt, GraphQLString } from "graphql";
export const typeMap = new Map(Object.entries({
    "ID": GraphQLID,
    "Symbol": GraphQLID,
    "Boolean": GraphQLBoolean,
    "Number": GraphQLFloat,
    "BigInt": GraphQLInt,
    "Int": GraphQLInt,
    "String": GraphQLString
}));
