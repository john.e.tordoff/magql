export const composeDecorators = (decorators)=>(...decoratorArgs)=>decorators.forEach((decorator)=>decorator(...decoratorArgs));
