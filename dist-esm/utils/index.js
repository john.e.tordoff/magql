export { entriesMap } from "./map-entries.js";
export { DefaultMap } from "./default-map.js";
export { composeDecorators } from "./compose-decorators.js";
export const isEmptyObject = (obj)=>{
    return JSON.stringify(obj) === JSON.stringify({});
};
export const isEmptyArray = (arr)=>{
    return Array.isArray(arr) && arr.length === 0;
};
export const isFn = (fn)=>{
    return typeof fn === "function";
};
export const isConstructor = (fn)=>{
    return Boolean(fn?.prototype?.constructor);
};
