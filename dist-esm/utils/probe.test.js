import { assert } from "https://deno.land/std@0.171.0/testing/asserts.js";
import { probe } from "./probe.js";
Deno.test("probe tracks accessed props", async ()=>{
    const fn = async ({ a , b  })=>a + b;
    const accessedProps = await probe(fn);
    assert(accessedProps.has("a"));
    assert(accessedProps.has("b"));
});
Deno.test("probe tracks accessed props any arg", async ()=>{
    const fn = async ({ a , b  })=>a + b;
    const accessedProps = await probe(fn);
    assert(accessedProps.has("a"));
    assert(accessedProps.has("b"));
});
