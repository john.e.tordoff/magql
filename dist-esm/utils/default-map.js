function _defineProperty(obj, key, value) {
    if (key in obj) {
        Object.defineProperty(obj, key, {
            value: value,
            enumerable: true,
            configurable: true,
            writable: true
        });
    } else {
        obj[key] = value;
    }
    return obj;
}
export class DefaultMap {
    has(key) {
        return this.map.has(key);
    }
    get(key) {
        if (this.has(key)) {
            return this.map.get(key);
        } else {
            const defaultValue = this.defaultValue();
            this.map.set(key, defaultValue);
            return defaultValue;
        }
    }
    forEach(eachFn) {
        this.map.forEach(eachFn);
    }
    constructor(defaultValue, iterable){
        _defineProperty(this, "defaultValue", void 0);
        _defineProperty(this, "map", void 0);
        this.defaultValue = defaultValue;
        this.defaultValue = defaultValue;
        this.map = new Map(iterable);
    }
}
