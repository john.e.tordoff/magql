const probed = new WeakMap();
export const probe = async (fn)=>{
    if (!probed.has(fn)) {
        const probedProps = new Set();
        const _probe = new Proxy({}, {
            get: (target, property)=>{
                probedProps.add(property);
            }
        });
        try {
            await fn(_probe);
        } catch (error) {
        // catch all errors.
        }
        probed.set(fn, probedProps);
    }
    return probed.get(fn);
};
