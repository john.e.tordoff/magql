import { composeDecorators } from "../utils/index.js";
import { operationType } from "./operation-type.js";
import { parameters } from "./parameters.js";
import { returns } from "./returns.js";
import { fieldName } from "./field-name.js";
import { liftResolver } from "./lift-resolver.js";
/**
 * A decorator function that marks a resolver function as a GraphQL operation.
 * The decorated resolver function will be registered in the appropriate map
 * (`queries`, `mutations`, or `subscriptions`) of the target class.
 *
 * @param _operationType The operation type for the decorated resolver function.
 * @param operationName The name of the decorated resolver function.
 * @param parametersArg The parameters for the decorated resolver function.
 * @param returnsArg The return value for the decorated resolver function.
 */ export const operation = (_operationType)=>{
    return ({ operationName , parameters: parametersArg , lift =()=>({}) , returns: returnsArg = ({ receiver  })=>[
            receiver
        ]  })=>{
        return composeDecorators([
            parameters(parametersArg),
            returns(returnsArg),
            fieldName(operationName),
            liftResolver(lift),
            operationType(_operationType)
        ]);
    };
};
