import { returns } from "./returns.js";
export const field = (fieldType)=>(target, property, descriptor)=>{
        returns(fieldType)(target, property, descriptor);
        const resolve = descriptor?.get ?? descriptor?.value;
        if (typeof resolve === "function") {
            descriptor.resolve = (_)=>resolve.call(_);
        } else {
            descriptor.resolve = ()=>resolve;
        }
        target.constructor.fields.set(property, descriptor);
    };
