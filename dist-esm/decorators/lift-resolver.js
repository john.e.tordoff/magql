/**
 * A decorator function that can be used to modify the behavior of a class 
 * method and make it compatible with use as a GraphQL resolver.
 *
 * @param {Function} mapping - A function that maps the input arguments
 *   and context of the original method to the arguments that should be
 *   passed to the resolver.
 *
 * @return {Function} A decorator function that can be used to modify the
 *   behavior of a class method.
 */ export const liftResolver = (mapping)=>{
    return (target, property, descriptor)=>{
        // Add a new method to the descriptor object of the original method.
        // This new method returns a GraphQL resolver when called with the
        // appropriate arguments.
        descriptor.liftResolver = ({ receiver , method  })=>{
            return (root, args, context, info)=>{
                // Call the original method with the result of calling the
                // mapping function, which is passed an object containing
                // information about the decorated method and its context.
                return method.call(receiver, mapping({
                    root,
                    args,
                    context,
                    info,
                    receiver,
                    method,
                    target,
                    property,
                    descriptor
                }));
            };
        };
    };
};
