import { camelize } from "inflected";
/**
 * A decorator function that sets the `name` property on the descriptor object
 * for a resolver function. The value of the `name` property is used as the
 * field name in the generated GraphQL schema.
 *
 * @param operationName The field name for the decorated resolver function.
 *   Can be a string, a function, or undefined.
 */ export const fieldName = (_name)=>(target, property, descriptor)=>{
        if (typeof _name === "string") {
            descriptor.name = ()=>_name;
        } else if (_name instanceof Function) {
            descriptor.name = ({ receiver  })=>_name({
                    operationType: descriptor.operationType,
                    property,
                    receiver,
                    target,
                    descriptor
                });
        } else if (typeof _name === "undefined") {
            descriptor.name = ({ receiver  })=>`${camelize(receiver.name, false)}${camelize(String(property))}`;
        }
    };
