import { MaGQLType } from "../lib/types/base.ts";
import { query, field, returns } from "../lib/decorators.ts";

export class TestType extends MaGQLType {

    @query({
        operationName: "getOne",
        returns: {}
    })
    static getOne() {
        return new TestType();
    }        

    @field(String)
    hello() {
        return "world";
    }

}

export const types = [
    TestType
];
