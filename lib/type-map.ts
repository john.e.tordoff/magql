import {
    GraphQLType,
    GraphQLID,
    GraphQLBoolean,
    GraphQLFloat,
    GraphQLInt,
    GraphQLString
} from "graphql";

export const typeMap = new Map<string, GraphQLType>(Object.entries({
    "ID": GraphQLID,
    "Symbol": GraphQLID,
    "Boolean": GraphQLBoolean,
    "Number": GraphQLFloat,
    "BigInt": GraphQLInt,
    "Int": GraphQLInt,
    "String": GraphQLString
}));
