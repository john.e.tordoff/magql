export class DefaultMap {
    defaultValue;
    map;
    has(key) {
        return this.map.has(key);
    }
    get(key) {
        if (this.has(key)) {
            return this.map.get(key);
        }
        else {
            const defaultValue = this.defaultValue();
            this.map.set(key, defaultValue);
            return defaultValue;
        }
    }
    forEach(eachFn) {
        this.map.forEach(eachFn);
    }
    constructor(defaultValue, iterable) {
        this.defaultValue = defaultValue;
        this.defaultValue = defaultValue;
        this.map = new Map(iterable);
    }
}
