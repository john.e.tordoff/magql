export type ElementOf<T> = T extends any[] ? T[number] : T;
export type MappingFn<T, U> = (val: ElementOf<T>) => ElementOf<U>;
export type MapRelativeToIndex<T> = (
    fn: MappingFn<T, T>,
    i: number
) => (arr: T) => T;

export const mapIndex = <T>(
    fn: MappingFn<T, T>,
    i: number
) => (arr: T) => {
    return (arr as any[]).map((val: ElementOf<T>, index: number) => {
        if (i === index) {
            return fn(val);
        } else {
            return val;
        }
    });
};

export const mapInitial = <T>(
    fn: MappingFn<T, T>,
    i: number
) => (arr: T) => ([
    ...(arr as any[]).slice(0, i).map(fn),
    ...(arr as any[]).slice(i)
]);
