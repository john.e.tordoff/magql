const probed = new WeakMap<Function, Set<PropertyKey>>();

export const probe = async (fn: any): Promise<Set<PropertyKey>> => {
    if (!probed.has(fn)) {
        const probedProps = new Set<PropertyKey>();
        const _probe = new Proxy({}, {
            get: (target, property) => {
                probedProps.add(property);
            }
        });
        try {
            await (fn as any)(_probe);
        } catch (error) {
            // catch all errors.
        }
        probed.set(fn, probedProps);
    }
    return probed.get(fn) as Set<PropertyKey>;
};
