export class DefaultMap<K, V> {

    private map: Map<K, V>;

    has(key: K) {
        return this.map.has(key);
    }

    get(key: K) : V {
        if (this.has(key)) {
            return this.map.get(key) as V;
        } else {
            const defaultValue = this.defaultValue();
            this.map.set(key, defaultValue);
            return defaultValue;
        }
    }

    forEach(eachFn: (value: V, key: K) => void): void {
        this.map.forEach(eachFn);
    }

    constructor(
        private defaultValue: () => V,
        iterable?: Iterable<[K, V]>
    ) {
        this.defaultValue = defaultValue;
        this.map = new Map<K, V>(iterable);
    }

}
