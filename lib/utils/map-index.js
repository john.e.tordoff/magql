export const mapIndex = (fn, i) => (arr) => {
    return arr.map((val, index) => {
        if (i === index) {
            return fn(val);
        }
        else {
            return val;
        }
    });
};
export const mapInitial = (fn, i) => (arr) => ([
    ...arr.slice(0, i).map(fn),
    ...arr.slice(i)
]);
