export { entriesMap } from "./map-entries.ts";
export { DefaultMap } from "./default-map.ts";
export { composeDecorators } from "./compose-decorators.ts";

export const isEmptyObject = (obj: any): boolean => {
    return JSON.stringify(obj) === JSON.stringify({});
};

export const isEmptyArray = (arr: any): boolean => {
    return Array.isArray(arr) && arr.length === 0;
};

export const isFn = (fn: any): boolean => {
    return typeof fn === "function";
};

export const isConstructor = (fn: Function): boolean => {
    return Boolean(fn?.prototype?.constructor);
};
