export { entriesMap } from "./map-entries.ts";
export { DefaultMap } from "./default-map.ts";
export { composeDecorators } from "./compose-decorators.ts";
export const isEmptyObject = (obj) => {
    return JSON.stringify(obj) === JSON.stringify({});
};
export const isEmptyArray = (arr) => {
    return Array.isArray(arr) && arr.length === 0;
};
export const isFn = (fn) => {
    return typeof fn === "function";
};
export const isConstructor = (fn) => {
    return Boolean(fn?.prototype?.constructor);
};
