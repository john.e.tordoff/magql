import { ElementOf } from "./map-index.ts";
import { entriesMap, Entry } from "./map-entries.ts";

//export const entriesMap = <T extends { [s: PropertyKey]: T[keyof T] }, U>(
declare global {
    interface ObjectConstructor {
        entriesMap<T extends { [k: PropertyKey]: T[keyof T] }, U>(
            target: T,
            mapFn: (entry: [PropertyKey, T[keyof T]]) => [PropertyKey, U[keyof U]] | []
        ): U
    }
}

Object.entriesMap = entriesMap;
