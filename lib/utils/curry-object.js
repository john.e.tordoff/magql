const curryObject = (fn, defaultKeys) => {
    const curried = (partialArgs = {}) => {
        return (fullArgs = {}) => {
            const args = { ...partialArgs, ...fullArgs };
            if (Object.keys(args).length >= defaultKeys.length) {
                return fn(args);
            }
            else {
                return curried(args);
            }
        };
    };
    return curried;
};
