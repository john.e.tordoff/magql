import { mapIndex } from "./map-index.ts";
export const mapRight = (fn) => mapIndex(fn, 1);
