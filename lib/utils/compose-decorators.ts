export const composeDecorators = (decorators: any[]) => (...decoratorArgs: any[]) =>
    decorators.forEach((decorator) => decorator(...decoratorArgs));

