export type Entry<T> = [PropertyKey, T];
export type MapEntry<T, U> = (entry: [PropertyKey, T]) => [PropertyKey, U] | [];

export const entriesMap = <T extends { [s: PropertyKey]: T[keyof T] }, U>(
    object: T,
    mapFn: MapEntry<T[keyof T], U[keyof U]>
): U => {
    const objectEntries = Object.entries(object);
    const mappedEntries = objectEntries.map(mapFn);
    return Object.fromEntries(mappedEntries) as U;
}
