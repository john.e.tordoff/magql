import { assert } from "https://deno.land/std@0.171.0/testing/asserts.ts";
import { probe } from "./probe.ts";

Deno.test("probe tracks accessed props", async () => {
    const fn = async ({ a, b }: { a: number; b: number; }): Promise<number> => a + b;
    const accessedProps = await probe(fn);
    assert(accessedProps.has("a"))
    assert(accessedProps.has("b"))
});

Deno.test("probe tracks accessed props any arg", async () => {
    const fn = async ({ a, b }: { a: number; b: number; }): Promise<number> => a + b;
    const accessedProps = await probe(fn);
    assert(accessedProps.has("a"))
    assert(accessedProps.has("b"))
});
