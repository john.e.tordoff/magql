
declare global {
    interface MapConstructor {
        empty<T, U>(): Map<T, U>
    }
}

Map.empty = () => new Map();
