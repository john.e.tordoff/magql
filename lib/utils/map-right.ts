import { MappingFn, mapIndex } from "./map-index.ts";
import { ElementOf } from "./map-index.ts";

export const mapRight = <T>(fn: (val: ElementOf<T>) => ElementOf<T>) =>
    mapIndex<T>(fn as MappingFn<T, T> , 1);
