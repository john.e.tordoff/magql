

const curryObject = (
    fn: (args: any) => any,
    defaultKeys: PropertyKey[]
) => {
    const curried = (partialArgs: any = {}) => {
        return (fullArgs: any = {}) => {
            const args = { ...partialArgs, ...fullArgs };
            if (Object.keys(args).length >= defaultKeys.length) {
                return fn(args);
            } else {
                return curried(args);
            }
        };
    };
    return curried;
}
