export const entriesMap = (object, mapFn) => {
    const objectEntries = Object.entries(object);
    const mappedEntries = objectEntries.map(mapFn);
    return Object.fromEntries(mappedEntries);
};
