
declare global {
    interface ObjectConstructor {
        mapEntries<T, U>(
            target: { [key: PropertyKey]: T },
            mapFn: ([PropertyKey, T]) => [PropertyKey, U]
        ): [PropertyKey, U][]
    }
}
