import "./utils/map-entries.polyfill.ts";
import { operation } from "./decorators/operation.ts";
export { returns } from "./decorators/returns.ts";
export { field } from "./decorators/field.ts";
export const args = (...argTypes) => {
    return (target, key, descriptor) => {
        descriptor.value.argTypes = argTypes.map((argType) => {
            return typeof argType === "string" ? argType : argType.name;
        });
        return descriptor;
    };
};
/**
 * A decorator function that marks a resolver function as a GraphQL query.
 * The decorated resolver function will be registered in the `queries` map of
 * the target class.
 */
export const query = operation("Query");
/**
 * A decorator function that marks a resolver function as a GraphQL mutation.
 * The decorated resolver function will be registered in the `mutations` map of
 * the target class.
 */
export const mutation = operation("Mutation");
/**
 * A decorator function that marks a resolver function as a GraphQL subscription.
 * The decorated resolver function will be registered in the `subscriptions`
 * map of the target class.
 */
export const subscription = operation("Subscription");
