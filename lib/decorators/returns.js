import { GraphQLList } from "graphql";
import { typeMap } from "../type-map.ts";
import { isEmptyObject, isEmptyArray, isConstructor } from "../utils/index.ts";
/**
 * Returns a decorator function that can be used to set the return type of a
 * class method or a static method in a class.
 *
 * @param returnsArg - The return type for the decorated method. Can be a
 * string or an anonymous function.
 *
 * @returns A decorator function that takes three arguments: target, property,
 * and descriptor.
 *
 * The decorator function adds a returns property to the descriptor object.
 * This property is a function that takes a single argument, receiver.
 *
 * The function determines the return type of the decorated method based on
 * the value of returnsArg. If returnsArg is an anonymous function, it is
 * called with an object containing the arguments receiver, target, property,
 * and descriptor. Otherwise, the value of returnsArg is used to determine
 * the return type.
 *
 * The return type is determined based on the type of returnsArg and whether
 * it is an array or an object. If it is an empty object, the type property
 * of the ReceivingClass is returned. If it is an empty array or an array
 * with an empty object, the arrayType property of the ReceivingClass is
 * returned. If it is an array, a GraphQLList is returned using the first
 * element of the array as the type. If it is not an array, the return type
 * is looked up in a typeMap using the name of returnsArg or the value of
 * returnsArg if it is a string.
 */
export const returns = (returnsArg) => {
    return (target, property, descriptor) => {
        // Determine whether the decorated method is a static method
        // or an instance method
        const isStatic = typeof target === "function" && isConstructor(target);
        // Add a returns property to the descriptor object
        descriptor.returns = ({ receiver }) => {
            // Determine the class that the decorated method belongs to
            // If the decorated method is static, the class is the target
            // If it is an instance method, it is the constructor of the
            // receiver object
            const ReceivingClass = isStatic ? receiver : receiver.constructor;
            const _returns = typeof returnsArg !== "function" || isConstructor(returnsArg)
                ? returnsArg
                : returnsArg({ receiver, target, property, descriptor });
            // If returnType is an empty object, return the type property
            // of ReceivingClass
            if (isEmptyObject(_returns)) {
                return ReceivingClass.type;
            }
            // If returnType is an array, 
            if (Array.isArray(_returns)) {
                // If returnType is an empty array or the array's element is
                // an empty object, return the arrayType property of
                // ReceivingClass
                if (isEmptyArray(_returns) || isEmptyObject(_returns[0])) {
                    return ReceivingClass.arrayType;
                }
                // Otherwise return a GraphQLList using the first (only)
                // element of the array as the type
                const typeName = typeof _returns[0] === "string"
                    ? _returns[0]
                    : _returns[0].name;
                return GraphQLList(typeMap.get(typeName));
            }
            // ReturnType is not an array, look up the type in the
            // typeMap using the name of returnType or the value of 
            // returnType if it is a string
            const typeName = typeof _returns === "string"
                ? _returns
                : _returns.name;
            return typeMap.get(typeName);
        };
    };
};
