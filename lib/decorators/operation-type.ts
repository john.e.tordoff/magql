import { pluralize } from "inflected";

/**
 * A decorator function that sets the `operationType` property on the
 * descriptor object and registers the decorated method in the field
 * map for the given operation type of the target class.
 *
 * @param _operationType The operation type for the decorated method.
 */
export const operationType = (_operationType: string) =>
    (
        target: any, 
        property: PropertyKey,
        descriptor: PropertyDescriptor
    ) => {
        // Define the `operationType` property on the descriptor object.
        Object.defineProperty(descriptor, "operationType", {
            value: _operationType,
            writable: false,
            enumerable: false,
            configurable: false
        });
        // Determine if the decorator was attached to a static method
        // or an instance method.
        const isStatic = target instanceof Function;
        // If the decorator was attached to a static method, the target
        // class is the target itself. Otherwise, the target class is the
        // constructor of the target.
        const TargetClass = isStatic ? target : target.constructor;
        // Register the decorated method in the field map for the given
        // operation type for the target class.
        const fieldMapPropKey = pluralize(_operationType.toLowerCase());
        TargetClass[fieldMapPropKey].set(property, descriptor);
    };

