import { returns } from "./returns.ts";
import { AnnotatedDescriptor } from "../types/base.ts";

export const field = (fieldType: any) =>
    (target: any, property: string, descriptor: AnnotatedDescriptor) => {
        returns(fieldType)(target, property, descriptor);
        const resolve = descriptor?.get ?? descriptor?.value;
        if (typeof resolve === "function") {
            descriptor.resolve = (_: any) => resolve.call(_);
        } else {
            descriptor.resolve = () => resolve;
        }
        target.constructor.fields.set(property, descriptor);
    };
