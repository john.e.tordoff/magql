import { typeMap } from "../type-map.ts";
import { mapRight } from "../utils/map-right.ts";
import "../utils/map-entries.polyfill.ts";
const makeTypeConfig = (type) => ({ type: typeMap.get(type.name) });
const buildParameters = (parameterTypeMap) => Object.entriesMap(parameterTypeMap, mapRight(makeTypeConfig));
export const parameters = (parametersArg) => {
    return (target, property, descriptor) => {
        descriptor.parameters = ({ receiver }) => {
            if (typeof parametersArg === "function") {
                return buildParameters(parametersArg({
                    receiver, property, descriptor
                }));
            }
            else {
                return {};
            }
        };
    };
};
