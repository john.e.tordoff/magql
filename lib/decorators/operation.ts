import { composeDecorators } from "../utils/index.ts";

import { operationType } from "./operation-type.ts";
import { parameters } from "./parameters.ts";
import { returns } from "./returns.ts";
import { FieldName, fieldName } from "./field-name.ts";
import { liftResolver } from "./lift-resolver.ts";

type OperationSignature = {
    operationName?: FieldName;
    parameters?: any;
    returns?: any;
    lift?: any;
};

/**
 * A decorator function that marks a resolver function as a GraphQL operation.
 * The decorated resolver function will be registered in the appropriate map
 * (`queries`, `mutations`, or `subscriptions`) of the target class.
 *
 * @param _operationType The operation type for the decorated resolver function.
 * @param operationName The name of the decorated resolver function.
 * @param parametersArg The parameters for the decorated resolver function.
 * @param returnsArg The return value for the decorated resolver function.
 */
export const operation = (_operationType: string) => {
    return ({ 
        operationName,
        parameters: parametersArg, 
        lift = () => ({}),
        returns: returnsArg = ({ receiver }: any) => [receiver]
    }: OperationSignature) => {
        return composeDecorators([
            parameters(parametersArg),
            returns(returnsArg),
            fieldName(operationName),
            liftResolver(lift),
            operationType(_operationType)
        ]);
    };
};
