import { GraphQLType } from "graphql";
import { typeMap } from "../type-map.ts";
import {
    MaGQLType,
    AnnotatedDescriptor
} from "../types/base.ts";
import { mapRight } from "../utils/map-right.ts";
import { MapEntry, Entry } from "../utils/map-entries.ts";
import "../utils/map-entries.polyfill.ts";

type PartialFieldConfig = {
    type: GraphQLType
};

const makeTypeConfig = (type: any): PartialFieldConfig  =>
    ({ type: typeMap.get(type.name) as GraphQLType });

const buildParameters = (parameterTypeMap: any) =>
    Object.entriesMap<any, any>(
        parameterTypeMap,
        mapRight<Entry<any>>(makeTypeConfig) as MapEntry<any, any>
    );

export const parameters = (parametersArg: Function | undefined) => {
    return (
        target: any, 
        property: PropertyKey, 
        descriptor: AnnotatedDescriptor
    ) => {
        descriptor.parameters = ({
            receiver
        }: {
            receiver: MaGQLType
        }) => {
            if (typeof parametersArg === "function") {
                return buildParameters(parametersArg({
                    receiver, property, descriptor
                }));
            } else {
                return {};
            }
        }           
    }; 
};
