import { pluralize } from "inflected";
import { GraphQLList, GraphQLObjectType } from "graphql";
import { DefaultMap } from "../utils/index.ts";
import "../utils/map-empty.polyfill.ts";
;
const typeMap = new DefaultMap(Map.empty);
const queryMap = new DefaultMap(Map.empty);
const mutationMap = new DefaultMap(Map.empty);
const subscriptionMap = new DefaultMap(Map.empty);
const fieldMap = new DefaultMap(Map.empty);
/**
 * The `AutoSchema` class is the base class for GraphQL types. It
 * provides a set of static methods that can be used to define and retrieve
 * the fields and queries of a GraphQL type.
 *
 * By using the `AutoSchema` class as a base class for their GraphQL types and
 * defining the fields and queries of their types using the `getFields` and
 * `getQueries` methods, developers can automatically generate a GraphQL
 * schema for their types. This can be useful for generating a schema on the
 * fly or for dynamically modifying a schema based on the data being queried.
 */
export class MaGQLType {
    static _type = null;
    static _arrayType = null;
    /**
     * Returns the GraphQLObjectType object for this class.
     *
     * The object type represents a group of fields in the GraphQL schema and is
     * created using lazy initialization, meaning it is only created when it is
     * first needed.
     *
     * @return {GraphQLObjectType} The GraphQLObjectType object for this class.
     */
    static get type() {
        if (!this._type) {
            this._type = new GraphQLObjectType({
                name: this.name,
                fields: this.getFieldConfigs()
            });
        }
        return this._type;
    }
    /**
     * Returns the GraphQLList object for this class.
     *
     * The GraphQLList object represents an array of objects in the GraphQL schema
     * and is created using lazy initialization, meaning it is only created when it
     * is first needed.
     *
     * @return {GraphQLList} The GraphQLList object for this class.
     */
    static get arrayType() {
        if (!this._arrayType) {
            this._arrayType = new GraphQLList(this.type);
        }
        return this._arrayType;
    }
    static get fields() {
        return fieldMap.get(this);
    }
    static get queries() {
        return queryMap.get(this);
    }
    static get mutations() {
        return mutationMap.get(this);
    }
    static get subscriptions() {
        return subscriptionMap.get(this);
    }
    /**
     * Returns a Map of resolvers for the given class and all of its ancestors.
     * @param map - Map to retrieve resolvers from.
     * @returns Map<string, any> - Map of resolvers.
     */
    static getFields(fieldType) {
        if (this === MaGQLType) {
            const baseFields = this[pluralize(fieldType.toLowerCase())];
            return baseFields;
        }
        // The result of getPrototypeOf can be null, but we know that can't be the
        // case since we already handled the case we have the root type, so here
        // casting it to the MaGQLConstructor is ok.
        const prototype = Reflect.getPrototypeOf(this);
        const ownFields = this[pluralize(fieldType.toLowerCase())];
        const ancestorFields = prototype?.getFields(fieldType) ?? [];
        const combinedFields = new Map([...ownFields, ...ancestorFields]);
        return combinedFields;
    }
    /**
     * Sets a field in the fieldMap
     */
    static registerField(fieldMap, property, descriptor) {
        fieldMap.get(this).set(property, descriptor);
    }
    static registerOwnField(property, descriptor) {
        this.registerField(fieldMap, property, descriptor);
    }
    static registerQuery(property, descriptor) {
        this.registerField(queryMap, property, descriptor);
    }
    static registerMutation(property, descriptor) {
        this.registerField(mutationMap, property, descriptor);
    }
    /**
     * Returns a map of field configurations for the fields of this class.
     *
     * The field configurations define the properties of the fields in a
     * GraphQL schema and are created by mapping the field descriptors to field
     * configurations using the `reduce` function.
     *
     * @return {GraphQLFieldConfigMap<any, any>} A map of field configurations
     * for the fields of this class.
     */
    static getFieldConfigs() {
        return Object.fromEntries([...this.getFields("Field").entries()]
            .map(([fieldName, { returns }]) => {
            return [
                fieldName,
                { type: returns({ receiver: this }) }
            ];
        }));
    }
}
