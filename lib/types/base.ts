import { camelize, pluralize } from "inflected";
import {
    GraphQLFieldConfigMap,
    GraphQLFieldConfig,
    GraphQLList,
    GraphQLObjectType
} from "graphql";

import { DefaultMap } from "../utils/index.ts";
import "../utils/map-empty.polyfill.ts";


export type DescriptorAnnotations = {
    type?: any;
    operationType?: any;
    returns?: any;
    parameters?: any;
    liftResolver?: any;
    resolve?: any;
    name?: any;
};
 
export type AnnotatedDescriptor = PropertyDescriptor & DescriptorAnnotations;
export type FieldEntry = [
    string,
    AnnotatedDescriptor
];

export interface MaGQLTypeConstructor {
    [key: PropertyKey]: any;
    getFields: any;
    fields: Map<PropertyKey, AnnotatedDescriptor>;
    queries: Map<PropertyKey, AnnotatedDescriptor>;
    mutations: Map<PropertyKey, AnnotatedDescriptor>;
    subscriptions: Map<PropertyKey, AnnotatedDescriptor>;
};

type DescriptorMap = Map<PropertyKey, AnnotatedDescriptor>;

const typeMap = new DefaultMap<MaGQLTypeConstructor, DescriptorMap>(Map.empty);
const queryMap = new DefaultMap<MaGQLTypeConstructor, DescriptorMap>(Map.empty);
const mutationMap = new DefaultMap<MaGQLTypeConstructor, DescriptorMap>(Map.empty);
const subscriptionMap = new DefaultMap<MaGQLTypeConstructor, DescriptorMap>(Map.empty);
const fieldMap = new DefaultMap<MaGQLTypeConstructor, DescriptorMap>(Map.empty);

/**
 * The `AutoSchema` class is the base class for GraphQL types. It
 * provides a set of static methods that can be used to define and retrieve
 * the fields and queries of a GraphQL type.
 *
 * By using the `AutoSchema` class as a base class for their GraphQL types and
 * defining the fields and queries of their types using the `getFields` and
 * `getQueries` methods, developers can automatically generate a GraphQL
 * schema for their types. This can be useful for generating a schema on the
 * fly or for dynamically modifying a schema based on the data being queried.
 */
export abstract class MaGQLType implements MaGQLType {

    private static _type: any = null;
    private static _arrayType: any = null;
  
    /**
     * Returns the GraphQLObjectType object for this class.
     *
     * The object type represents a group of fields in the GraphQL schema and is
     * created using lazy initialization, meaning it is only created when it is
     * first needed.
     *
     * @return {GraphQLObjectType} The GraphQLObjectType object for this class.
     */
    static get type() {
        if (!this._type) {
            this._type = new GraphQLObjectType({
                name: this.name,
                fields: this.getFieldConfigs()
            });
        }
        return this._type;
    }
  
    /**
     * Returns the GraphQLList object for this class.
     *
     * The GraphQLList object represents an array of objects in the GraphQL schema
     * and is created using lazy initialization, meaning it is only created when it
     * is first needed.
     *
     * @return {GraphQLList} The GraphQLList object for this class.
     */
    static get arrayType() {
        if (!this._arrayType) {
            this._arrayType = new GraphQLList(this.type);
        }
        return this._arrayType;
    }

    static get fields(): Map<PropertyKey, AnnotatedDescriptor> {
        return fieldMap.get(this);
    }
    
    static get queries(): Map<PropertyKey, AnnotatedDescriptor> {
        return queryMap.get(this);
    }
    
    static get mutations(): Map<PropertyKey, AnnotatedDescriptor> {
        return mutationMap.get(this);
    }
    static get subscriptions(): Map<PropertyKey, AnnotatedDescriptor>{
        return subscriptionMap.get(this);
    }

    /**
     * Returns a Map of resolvers for the given class and all of its ancestors.
     * @param map - Map to retrieve resolvers from.
     * @returns Map<string, any> - Map of resolvers.
     */
    static getFields(fieldType: string) {
        if (this === MaGQLType) {
            const baseFields = this[pluralize(fieldType.toLowerCase()) as keyof typeof MaGQLType];
            return baseFields;
        }
        // The result of getPrototypeOf can be null, but we know that can't be the
        // case since we already handled the case we have the root type, so here
        // casting it to the MaGQLConstructor is ok.
        const prototype: MaGQLTypeConstructor = Reflect.getPrototypeOf(this) as typeof MaGQLType;
        const ownFields = this[pluralize(fieldType.toLowerCase()) as keyof typeof MaGQLType]

        const ancestorFields = prototype?.getFields(fieldType) ?? [];
        const combinedFields = new Map([ ...ownFields, ...ancestorFields ]);
        return combinedFields;
    }
 
    /**
     * Sets a field in the fieldMap
     */
    static registerField(
        fieldMap: DefaultMap<any, any>,
        property: PropertyKey,
        descriptor: PropertyDescriptor
    ) {
        fieldMap.get(this).set(property, descriptor);
    }

    static registerOwnField(
        property: PropertyKey,
        descriptor: PropertyDescriptor
    ) {
        this.registerField(fieldMap, property, descriptor);
    }
    static registerQuery(
        property: string,
        descriptor: PropertyDescriptor
    ) {
        this.registerField(queryMap, property, descriptor);
    }
    static registerMutation(property: PropertyKey, descriptor: PropertyDescriptor) {
        this.registerField(mutationMap, property, descriptor);
    }

    /**
     * Returns a map of field configurations for the fields of this class.
     *
     * The field configurations define the properties of the fields in a
     * GraphQL schema and are created by mapping the field descriptors to field
     * configurations using the `reduce` function.
     *
     * @return {GraphQLFieldConfigMap<any, any>} A map of field configurations
     * for the fields of this class.
     */
    static getFieldConfigs(): GraphQLFieldConfigMap<any, any> {
        return Object.fromEntries(
            [...this.getFields("Field").entries()]
                .map(([ fieldName, { returns } ]: FieldEntry) => {
                    return [
                        fieldName,
                        { type: returns({ receiver: this}) } as GraphQLFieldConfig<any, any>
                    ];
                })            
        ) as GraphQLFieldConfigMap<any, any>
    }

}

