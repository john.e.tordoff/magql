"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "typeMap", {
    enumerable: true,
    get: function() {
        return typeMap;
    }
});
var _graphql = require("graphql");
var typeMap = new Map(Object.entries({
    "ID": _graphql.GraphQLID,
    "Symbol": _graphql.GraphQLID,
    "Boolean": _graphql.GraphQLBoolean,
    "Number": _graphql.GraphQLFloat,
    "BigInt": _graphql.GraphQLInt,
    "Int": _graphql.GraphQLInt,
    "String": _graphql.GraphQLString
}));
