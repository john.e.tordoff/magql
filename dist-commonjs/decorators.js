"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
function _export(target, all) {
    for(var name in all)Object.defineProperty(target, name, {
        enumerable: true,
        get: all[name]
    });
}
_export(exports, {
    returns: function() {
        return _returnsJs.returns;
    },
    field: function() {
        return _fieldJs.field;
    },
    args: function() {
        return args;
    },
    query: function() {
        return query;
    },
    mutation: function() {
        return mutation;
    },
    subscription: function() {
        return subscription;
    }
});
require("./utils/map-entries.polyfill.js");
var _operationJs = require("./decorators/operation.js");
var _returnsJs = require("./decorators/returns.js");
var _fieldJs = require("./decorators/field.js");
var args = function() {
    for(var _len = arguments.length, argTypes = new Array(_len), _key = 0; _key < _len; _key++){
        argTypes[_key] = arguments[_key];
    }
    return function(target, key, descriptor) {
        descriptor.value.argTypes = argTypes.map(function(argType) {
            return typeof argType === "string" ? argType : argType.name;
        });
        return descriptor;
    };
};
var query = (0, _operationJs.operation)("Query");
var mutation = (0, _operationJs.operation)("Mutation");
var subscription = (0, _operationJs.operation)("Subscription");
