"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
function _export(target, all) {
    for(var name in all)Object.defineProperty(target, name, {
        enumerable: true,
        get: all[name]
    });
}
_export(exports, {
    buildFieldConfig: function() {
        return buildFieldConfig;
    },
    buildFieldsForReceiver: function() {
        return buildFieldsForReceiver;
    },
    aggregateFields: function() {
        return aggregateFields;
    },
    createRootType: function() {
        return createRootType;
    },
    createSchema: function() {
        return createSchema;
    },
    summonSchema: function() {
        return summonSchema;
    }
});
var _graphql = require("graphql");
require("./utils/map-entries.polyfill.js");
function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for(var i = 0, arr2 = new Array(len); i < len; i++)arr2[i] = arr[i];
    return arr2;
}
function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
}
function _iterableToArrayLimit(arr, i) {
    var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"];
    if (_i == null) return;
    var _arr = [];
    var _n = true;
    var _d = false;
    var _s, _e;
    try {
        for(_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true){
            _arr.push(_s.value);
            if (i && _arr.length === i) break;
        }
    } catch (err) {
        _d = true;
        _e = err;
    } finally{
        try {
            if (!_n && _i["return"] != null) _i["return"]();
        } finally{
            if (_d) throw _e;
        }
    }
    return _arr;
}
function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}
function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(n);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}
var AggPropsMethodName;
(function(AggPropsMethodName) {
    AggPropsMethodName["queries"] = "queries";
    AggPropsMethodName["mutations"] = "mutations";
    AggPropsMethodName["subscriptions"] = "subscriptions";
})(AggPropsMethodName || (AggPropsMethodName = {}));
var buildFieldConfig = function(param) {
    var receiver = param.receiver, propertyName = param.propertyName, descriptor = param.descriptor;
    var args = descriptor.parameters({
        receiver: receiver
    });
    var returns = descriptor.returns({
        receiver: receiver
    });
    var resolve = descriptor.liftResolver({
        receiver: receiver,
        method: descriptor.value
    });
    return {
        type: returns,
        args: args,
        resolve: resolve
    };
};
var buildFieldsForReceiver = function(aggPropsMethodName) {
    return function(receiver) {
        var props = receiver.getFields(aggPropsMethodName);
        var fieldConfigFromProp = function(param) {
            var _param = _slicedToArray(param, 2), propertyName = _param[0], descriptor = _param[1];
            return [
                descriptor.name({
                    receiver: receiver
                }),
                buildFieldConfig({
                    receiver: receiver,
                    propertyName: propertyName,
                    descriptor: descriptor
                })
            ];
        };
        var builtFields = Array.from(props, fieldConfigFromProp);
        return builtFields;
    };
};
var aggregateFields = function(aggPropsMethodName, types) {
    var mappedProps = types.flatMap(buildFieldsForReceiver(aggPropsMethodName));
    var reducedProps = Object.fromEntries(mappedProps);
    return reducedProps;
};
var createRootType = function(types) {
    return function(param) {
        var _param = _slicedToArray(param, 2), rootTypeName = _param[0], aggPropsMethodName = _param[1];
        var fields = aggregateFields(aggPropsMethodName, types);
        if (Object.keys(fields).length > 0) {
            return [
                rootTypeName.toLowerCase(),
                new _graphql.GraphQLObjectType({
                    name: rootTypeName,
                    fields: fields
                })
            ];
        }
        return [];
    };
};
var createSchema = function(types) {
    var schemaConfig = Object.entriesMap({
        Query: AggPropsMethodName.queries,
        Mutation: AggPropsMethodName.mutations,
        Subscription: AggPropsMethodName.subscriptions
    }, createRootType(types));
    return new _graphql.GraphQLSchema(schemaConfig);
};
var summonSchema = createSchema;
