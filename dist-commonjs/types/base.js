"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "MaGQLType", {
    enumerable: true,
    get: function() {
        return MaGQLType;
    }
});
var _inflected = require("inflected");
var _graphql = require("graphql");
var _indexJs = require("../utils/index.js");
require("../utils/map-empty.polyfill.js");
function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for(var i = 0, arr2 = new Array(len); i < len; i++)arr2[i] = arr[i];
    return arr2;
}
function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
}
function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}
function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}
function _defineProperties(target, props) {
    for(var i = 0; i < props.length; i++){
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
    }
}
function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
}
function _iterableToArray(iter) {
    if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}
function _iterableToArrayLimit(arr, i) {
    var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"];
    if (_i == null) return;
    var _arr = [];
    var _n = true;
    var _d = false;
    var _s, _e;
    try {
        for(_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true){
            _arr.push(_s.value);
            if (i && _arr.length === i) break;
        }
    } catch (err) {
        _d = true;
        _e = err;
    } finally{
        try {
            if (!_n && _i["return"] != null) _i["return"]();
        } finally{
            if (_d) throw _e;
        }
    }
    return _arr;
}
function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}
function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}
function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(n);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}
var typeMap = new _indexJs.DefaultMap(Map.empty);
var queryMap = new _indexJs.DefaultMap(Map.empty);
var mutationMap = new _indexJs.DefaultMap(Map.empty);
var subscriptionMap = new _indexJs.DefaultMap(Map.empty);
var fieldMap = new _indexJs.DefaultMap(Map.empty);
var MaGQLType = /*#__PURE__*/ function() {
    "use strict";
    function MaGQLType() {
        _classCallCheck(this, MaGQLType);
    }
    /**
     * Returns a Map of resolvers for the given class and all of its ancestors.
     * @param map - Map to retrieve resolvers from.
     * @returns Map<string, any> - Map of resolvers.
     */ MaGQLType.getFields = function getFields(fieldType) {
        if (this === MaGQLType) {
            var baseFields = this[(0, _inflected.pluralize)(fieldType.toLowerCase())];
            return baseFields;
        }
        // The result of getPrototypeOf can be null, but we know that can't be the
        // case since we already handled the case we have the root type, so here
        // casting it to the MaGQLConstructor is ok.
        var prototype = Reflect.getPrototypeOf(this);
        var ownFields = this[(0, _inflected.pluralize)(fieldType.toLowerCase())];
        var _prototype_getFields;
        var ancestorFields = (_prototype_getFields = prototype === null || prototype === void 0 ? void 0 : prototype.getFields(fieldType)) !== null && _prototype_getFields !== void 0 ? _prototype_getFields : [];
        var combinedFields = new Map(_toConsumableArray(ownFields).concat(_toConsumableArray(ancestorFields)));
        return combinedFields;
    };
    /**
     * Sets a field in the fieldMap
     */ MaGQLType.registerField = function registerField(fieldMap, property, descriptor) {
        fieldMap.get(this).set(property, descriptor);
    };
    MaGQLType.registerOwnField = function registerOwnField(property, descriptor) {
        this.registerField(fieldMap, property, descriptor);
    };
    MaGQLType.registerQuery = function registerQuery(property, descriptor) {
        this.registerField(queryMap, property, descriptor);
    };
    MaGQLType.registerMutation = function registerMutation(property, descriptor) {
        this.registerField(mutationMap, property, descriptor);
    };
    /**
     * Returns a map of field configurations for the fields of this class.
     *
     * The field configurations define the properties of the fields in a
     * GraphQL schema and are created by mapping the field descriptors to field
     * configurations using the `reduce` function.
     *
     * @return {GraphQLFieldConfigMap<any, any>} A map of field configurations
     * for the fields of this class.
     */ MaGQLType.getFieldConfigs = function getFieldConfigs() {
        var _this = this;
        return Object.fromEntries(_toConsumableArray(this.getFields("Field").entries()).map(function(param) {
            var _param = _slicedToArray(param, 2), fieldName = _param[0], returns = _param[1].returns;
            return [
                fieldName,
                {
                    type: returns({
                        receiver: _this
                    })
                }
            ];
        }));
    };
    _createClass(MaGQLType, null, [
        {
            key: "type",
            get: /**
     * Returns the GraphQLObjectType object for this class.
     *
     * The object type represents a group of fields in the GraphQL schema and is
     * created using lazy initialization, meaning it is only created when it is
     * first needed.
     *
     * @return {GraphQLObjectType} The GraphQLObjectType object for this class.
     */ function get() {
                if (!this._type) {
                    this._type = new _graphql.GraphQLObjectType({
                        name: this.name,
                        fields: this.getFieldConfigs()
                    });
                }
                return this._type;
            }
        },
        {
            key: "arrayType",
            get: /**
     * Returns the GraphQLList object for this class.
     *
     * The GraphQLList object represents an array of objects in the GraphQL schema
     * and is created using lazy initialization, meaning it is only created when it
     * is first needed.
     *
     * @return {GraphQLList} The GraphQLList object for this class.
     */ function get() {
                if (!this._arrayType) {
                    this._arrayType = new _graphql.GraphQLList(this.type);
                }
                return this._arrayType;
            }
        },
        {
            key: "fields",
            get: function get() {
                return fieldMap.get(this);
            }
        },
        {
            key: "queries",
            get: function get() {
                return queryMap.get(this);
            }
        },
        {
            key: "mutations",
            get: function get() {
                return mutationMap.get(this);
            }
        },
        {
            key: "subscriptions",
            get: function get() {
                return subscriptionMap.get(this);
            }
        }
    ]);
    return MaGQLType;
}();
MaGQLType._type = null;
MaGQLType._arrayType = null;
