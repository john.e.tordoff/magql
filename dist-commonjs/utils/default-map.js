"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "DefaultMap", {
    enumerable: true,
    get: function() {
        return DefaultMap;
    }
});
function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}
var DefaultMap = /*#__PURE__*/ function() {
    "use strict";
    function DefaultMap(defaultValue, iterable) {
        _classCallCheck(this, DefaultMap);
        this.defaultValue = defaultValue;
        this.defaultValue = defaultValue;
        this.map = new Map(iterable);
    }
    var _proto = DefaultMap.prototype;
    _proto.has = function has(key) {
        return this.map.has(key);
    };
    _proto.get = function get(key) {
        if (this.has(key)) {
            return this.map.get(key);
        } else {
            var defaultValue = this.defaultValue();
            this.map.set(key, defaultValue);
            return defaultValue;
        }
    };
    _proto.forEach = function forEach(eachFn) {
        this.map.forEach(eachFn);
    };
    return DefaultMap;
}();
