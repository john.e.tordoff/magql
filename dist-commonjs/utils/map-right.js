"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "mapRight", {
    enumerable: true,
    get: function() {
        return mapRight;
    }
});
var _mapIndexJs = require("./map-index.js");
var mapRight = function(fn) {
    return (0, _mapIndexJs.mapIndex)(fn, 1);
};
