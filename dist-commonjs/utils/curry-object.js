"use strict";
function _defineProperty(obj, key, value) {
    if (key in obj) {
        Object.defineProperty(obj, key, {
            value: value,
            enumerable: true,
            configurable: true,
            writable: true
        });
    } else {
        obj[key] = value;
    }
    return obj;
}
function _objectSpread(target) {
    for(var i = 1; i < arguments.length; i++){
        var source = arguments[i] != null ? arguments[i] : {};
        var ownKeys = Object.keys(source);
        if (typeof Object.getOwnPropertySymbols === "function") {
            ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function(sym) {
                return Object.getOwnPropertyDescriptor(source, sym).enumerable;
            }));
        }
        ownKeys.forEach(function(key) {
            _defineProperty(target, key, source[key]);
        });
    }
    return target;
}
var curryObject = function(fn, defaultKeys) {
    var curried = function() {
        var partialArgs = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : {};
        return function() {
            var fullArgs = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : {};
            var args = _objectSpread({}, partialArgs, fullArgs);
            if (Object.keys(args).length >= defaultKeys.length) {
                return fn(args);
            } else {
                return curried(args);
            }
        };
    };
    return curried;
};
