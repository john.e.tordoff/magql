"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "entriesMap", {
    enumerable: true,
    get: function() {
        return entriesMap;
    }
});
var entriesMap = function(object, mapFn) {
    var objectEntries = Object.entries(object);
    var mappedEntries = objectEntries.map(mapFn);
    return Object.fromEntries(mappedEntries);
};
