"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
function _export(target, all) {
    for(var name in all)Object.defineProperty(target, name, {
        enumerable: true,
        get: all[name]
    });
}
_export(exports, {
    entriesMap: function() {
        return _mapEntriesJs.entriesMap;
    },
    DefaultMap: function() {
        return _defaultMapJs.DefaultMap;
    },
    composeDecorators: function() {
        return _composeDecoratorsJs.composeDecorators;
    },
    isEmptyObject: function() {
        return isEmptyObject;
    },
    isEmptyArray: function() {
        return isEmptyArray;
    },
    isFn: function() {
        return isFn;
    },
    isConstructor: function() {
        return isConstructor;
    }
});
var _mapEntriesJs = require("./map-entries.js");
var _defaultMapJs = require("./default-map.js");
var _composeDecoratorsJs = require("./compose-decorators.js");
var isEmptyObject = function(obj) {
    return JSON.stringify(obj) === JSON.stringify({});
};
var isEmptyArray = function(arr) {
    return Array.isArray(arr) && arr.length === 0;
};
var isFn = function(fn) {
    return typeof fn === "function";
};
var isConstructor = function(fn) {
    var _fn_prototype;
    return Boolean(fn === null || fn === void 0 ? void 0 : (_fn_prototype = fn.prototype) === null || _fn_prototype === void 0 ? void 0 : _fn_prototype.constructor);
};
