"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "liftResolver", {
    enumerable: true,
    get: function() {
        return liftResolver;
    }
});
var liftResolver = function(mapping) {
    return function(target, property, descriptor) {
        // Add a new method to the descriptor object of the original method.
        // This new method returns a GraphQL resolver when called with the
        // appropriate arguments.
        descriptor.liftResolver = function(param) {
            var receiver = param.receiver, method = param.method;
            return function(root, args, context, info) {
                // Call the original method with the result of calling the
                // mapping function, which is passed an object containing
                // information about the decorated method and its context.
                return method.call(receiver, mapping({
                    root: root,
                    args: args,
                    context: context,
                    info: info,
                    receiver: receiver,
                    method: method,
                    target: target,
                    property: property,
                    descriptor: descriptor
                }));
            };
        };
    };
};
         return method.call(receiver, mapping({
                    root: root,
                    args: args,
                    context: context,
                    info: info,
                    receiver: receiver,
                    method: method,
                    target: target,
                    property: property,
                    descriptor: descriptor
                }));
            };
        };
    };
};
