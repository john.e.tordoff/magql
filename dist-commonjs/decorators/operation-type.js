"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "operationType", {
    enumerable: true,
    get: function() {
        return operationType;
    }
});
var _inflected = require("inflected");
function _instanceof(left, right) {
    if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) {
        return !!right[Symbol.hasInstance](left);
    } else {
        return left instanceof right;
    }
}
var operationType = function(_operationType) {
    return function(target, property, descriptor) {
        // Define the `operationType` property on the descriptor object.
        Object.defineProperty(descriptor, "operationType", {
            value: _operationType,
            writable: false,
            enumerable: false,
            configurable: false
        });
        // Determine if the decorator was attached to a static method
        // or an instance method.
        var isStatic = _instanceof(target, Function);
        // If the decorator was attached to a static method, the target
        // class is the target itself. Otherwise, the target class is the
        // constructor of the target.
        var TargetClass = isStatic ? target : target.constructor;
        // Register the decorated method in the field map for the given
        // operation type for the target class.
        var fieldMapPropKey = (0, _inflected.pluralize)(_operationType.toLowerCase());
        TargetClass[fieldMapPropKey].set(property, descriptor);
    };
};
