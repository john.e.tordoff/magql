"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "field", {
    enumerable: true,
    get: function() {
        return field;
    }
});
var _returnsJs = require("./returns.js");
var field = function(fieldType) {
    return function(target, property, descriptor) {
        (0, _returnsJs.returns)(fieldType)(target, property, descriptor);
        var _descriptor_get;
        var resolve = (_descriptor_get = descriptor === null || descriptor === void 0 ? void 0 : descriptor.get) !== null && _descriptor_get !== void 0 ? _descriptor_get : descriptor === null || descriptor === void 0 ? void 0 : descriptor.value;
        if (typeof resolve === "function") {
            descriptor.resolve = function(_) {
                return resolve.call(_);
            };
        } else {
            descriptor.resolve = function() {
                return resolve;
            };
        }
        target.constructor.fields.set(property, descriptor);
    };
};
