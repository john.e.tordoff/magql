"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "operation", {
    enumerable: true,
    get: function() {
        return operation;
    }
});
var _indexJs = require("../utils/index.js");
var _operationTypeJs = require("./operation-type.js");
var _parametersJs = require("./parameters.js");
var _returnsJs = require("./returns.js");
var _fieldNameJs = require("./field-name.js");
var _liftResolverJs = require("./lift-resolver.js");
var operation = function(_operationType) {
    return function(param) {
        var operationName = param.operationName, parametersArg = param.parameters, _param_lift = param.lift, lift = _param_lift === void 0 ? function() {
            return {};
        } : _param_lift, tmp = param.returns, returnsArg = tmp === void 0 ? function(param) {
            var receiver = param.receiver;
            return [
                receiver
            ];
        } : tmp;
        return (0, _indexJs.composeDecorators)([
            (0, _parametersJs.parameters)(parametersArg),
            (0, _returnsJs.returns)(returnsArg),
            (0, _fieldNameJs.fieldName)(operationName),
            (0, _liftResolverJs.liftResolver)(lift),
            (0, _operationTypeJs.operationType)(_operationType)
        ]);
    };
};
