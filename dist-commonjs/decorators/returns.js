"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "returns", {
    enumerable: true,
    get: function() {
        return returns;
    }
});
var _graphql = require("graphql");
var _typeMapJs = require("../type-map.js");
var _indexJs = require("../utils/index.js");
var returns = function(returnsArg) {
    return function(target, property, descriptor) {
        // Determine whether the decorated method is a static method
        // or an instance method
        var isStatic = typeof target === "function" && (0, _indexJs.isConstructor)(target);
        // Add a returns property to the descriptor object
        descriptor.returns = function(param) {
            var receiver = param.receiver;
            // Determine the class that the decorated method belongs to
            // If the decorated method is static, the class is the target
            // If it is an instance method, it is the constructor of the
            // receiver object
            var ReceivingClass = isStatic ? receiver : receiver.constructor;
            var _returns = typeof returnsArg !== "function" || (0, _indexJs.isConstructor)(returnsArg) ? returnsArg : returnsArg({
                receiver: receiver,
                target: target,
                property: property,
                descriptor: descriptor
            });
            // If returnType is an empty object, return the type property
            // of ReceivingClass
            if ((0, _indexJs.isEmptyObject)(_returns)) {
                return ReceivingClass.type;
            }
            // If returnType is an array, 
            if (Array.isArray(_returns)) {
                // If returnType is an empty array or the array's element is
                // an empty object, return the arrayType property of
                // ReceivingClass
                if ((0, _indexJs.isEmptyArray)(_returns) || (0, _indexJs.isEmptyObject)(_returns[0])) {
                    return ReceivingClass.arrayType;
                }
                // Otherwise return a GraphQLList using the first (only)
                // element of the array as the type
                var typeName = typeof _returns[0] === "string" ? _returns[0] : _returns[0].name;
                return (0, _graphql.GraphQLList)(_typeMapJs.typeMap.get(typeName));
            }
            // ReturnType is not an array, look up the type in the
            // typeMap using the name of returnType or the value of 
            // returnType if it is a string
            var typeName1 = typeof _returns === "string" ? _returns : _returns.name;
            return _typeMapJs.typeMap.get(typeName1);
        };
    };
};
