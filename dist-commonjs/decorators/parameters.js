"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "parameters", {
    enumerable: true,
    get: function() {
        return parameters;
    }
});
var _typeMapJs = require("../type-map.js");
var _mapRightJs = require("../utils/map-right.js");
require("../utils/map-entries.polyfill.js");
var makeTypeConfig = function(type) {
    return {
        type: _typeMapJs.typeMap.get(type.name)
    };
};
var buildParameters = function(parameterTypeMap) {
    return Object.entriesMap(parameterTypeMap, (0, _mapRightJs.mapRight)(makeTypeConfig));
};
var parameters = function(parametersArg) {
    return function(target, property, descriptor) {
        descriptor.parameters = function(param) {
            var receiver = param.receiver;
            if (typeof parametersArg === "function") {
                return buildParameters(parametersArg({
                    receiver: receiver,
                    property: property,
                    descriptor: descriptor
                }));
            } else {
                return {};
            }
        };
    };
};
