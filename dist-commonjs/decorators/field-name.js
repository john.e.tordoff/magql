"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "fieldName", {
    enumerable: true,
    get: function() {
        return fieldName;
    }
});
var _inflected = require("inflected");
function _instanceof(left, right) {
    if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) {
        return !!right[Symbol.hasInstance](left);
    } else {
        return left instanceof right;
    }
}
var fieldName = function(_name) {
    return function(target, property, descriptor) {
        if (typeof _name === "string") {
            descriptor.name = function() {
                return _name;
            };
        } else if (_instanceof(_name, Function)) {
            descriptor.name = function(param) {
                var receiver = param.receiver;
                return _name({
                    operationType: descriptor.operationType,
                    property: property,
                    receiver: receiver,
                    target: target,
                    descriptor: descriptor
                });
            };
        } else if (typeof _name === "undefined") {
            descriptor.name = function(param) {
                var receiver = param.receiver;
                return "".concat((0, _inflected.camelize)(receiver.name, false)).concat((0, _inflected.camelize)(String(property)));
            };
        }
    };
};
