# MaGQL



It should be low cost for a developer to add a graphql api to a set of datatypes. It should be so easy that it seem _magical_.

Since it's quite common to use classes to represent a datatype, with instance methods for row-level operations and static methods for table level operations. This package exports a few decorators that can be attached to the mothods in those classes, and a utility function `summonSchema` which takes a list of those types that should be included in the schema, returning a graphql schema.

## Demo

A demo of this package in action has been put together on code sandbox.

https://codesandbox.io/p/sandbox/solitary-monad-2wisie?file=%2Findex.ts&selection=%5B%7B%22endColumn%22%3A36%2C%22endLineNumber%22%3A24%2C%22startColumn%22%3A36%2C%22startLineNumber%22%3A24%7D%5D
