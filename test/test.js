var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
[ /*]: # (*/]; /*)

```javascript

//*/
import { assertEquals } from "asserts";
import { GraphQLString, execute } from "graphql";
import { gql } from "graphql-tag";
import { query, field } from "../lib/decorators.ts";
import { summonSchema } from "../lib/schema.ts";
import { MaGQLType } from "../lib/types/base.ts";
Deno.test("Can Summon a schema", async () => {
    class TestType extends MaGQLType {
        static get queries() {
            return new Map([
                ["hello", {
                        name: () => "hello",
                        returns: () => GraphQLString,
                        parameters: () => undefined,
                        args: () => { },
                        liftResolver: () => () => "world"
                    }]
            ]);
        }
    }
    const schema = summonSchema([TestType]);
    const actual = execute({
        document: gql `
            query {
               hello
            }
        `,
        schema
    });
    const expected = {
        data: {
            hello: "world"
        }
    };
    assertEquals(actual, expected);
});
Deno.test("Can summon a schema using a @query decorator", async () => {
    class TestType extends MaGQLType {
        static hello() {
            return "world";
        }
    }
    __decorate([
        query({
            operationName: "hello",
            returns: String
        })
    ], TestType, "hello", null);
    const schema = summonSchema([TestType]);
    const actual = execute({
        schema,
        document: gql `
            query {
               hello
            }
        `
    });
    const expected = {
        data: {
            hello: "world"
        }
    };
    assertEquals(actual, expected);
});
/*
```

# Field Decorator

The following example shows the use of a field decorator. It takes one argument
that is used as the GraphQL return type. (It wraps the returns decorator).

The other thing that the field decorator does is handle the situations where
the property being decorated is an instance attribute, or is a property that has
getter associated with it, but the decorator also handles the situation where the
decorated property is actually a method.

The decorator will wrap the value or funtion in each of these cases approproately
such that when the resolver is called we correctly evaluate the value we should
return.

```javascript
//*/
Deno.test("Can summon a schema with collected fields", async () => {
    class TestType extends MaGQLType {
        static getOne() {
            return new TestType();
        }
        hello() {
            return "world";
        }
    }
    __decorate([
        field(String)
    ], TestType.prototype, "hello", null);
    __decorate([
        query({
            operationName: "getOne",
            returns: {}
        })
    ], TestType, "getOne", null);
    const schema = summonSchema([TestType]);
    const actual = await execute({
        schema,
        document: gql `
            query {
               getOne {
                   hello
               }
            }
        `
    });
    const expected = {
        data: {
            getOne: {
                hello: "world"
            }
        }
    };
    assertEquals(actual, expected);
});
/*
```

[//]: # (*/ //)
